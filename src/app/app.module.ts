import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AgGridModule} from "ag-grid-angular";
import {GridModule} from '@progress/kendo-angular-grid';
import {ButtonModule} from "@progress/kendo-angular-buttons";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {DialogModule} from "@progress/kendo-angular-dialog";
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import {appReducer} from "./shared/store/app.reducer";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AgGridModule,
    ToastrModule.forRoot(
      {
        timeOut: 3500,
        positionClass: 'toast-bottom-right',
        preventDuplicates: true
      }
    ),
    GridModule,
    ReactiveFormsModule,
    ButtonModule,
    FontAwesomeModule,
    DialogModule,
    StoreModule.forRoot( {myappstate: appReducer}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
  ],
  providers:[],
  bootstrap: [AppComponent]
})
export class AppModule {

}
