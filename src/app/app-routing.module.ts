import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {OrdersModule} from "./orders/orders.module";


const routes: Routes = [
  {
    path: '',
    loadChildren:() => import('./orders/orders.module').then(o => o.OrdersModule)
  },

  {
    path: '',
    redirectTo: 'order',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
