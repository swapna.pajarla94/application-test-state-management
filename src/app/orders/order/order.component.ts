import {Component, OnInit, Input} from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {
  PageChangeEvent,
  AddEvent,
  EditEvent,
  GridComponent,
  SaveEvent,
  RemoveEvent,
  CancelEvent
} from "@progress/kendo-angular-grid";
import {SortDescriptor} from "@progress/kendo-data-query";
import {faEdit, faClose} from "@fortawesome/free-solid-svg-icons";
import {ToastService} from "../toast.service";
import {OrdersService} from "../orders.service";
import {select, Store} from "@ngrx/store";
import {Order} from "../store/order";
import {invokeDeleteOrderAPI, invokeOrdersAPI, invokeSaveOrderAPI, invokeUpdateOrderAPI} from "../store/order.action";
import {selectOrders} from "../store/order.selector";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {selectAppState} from "../../shared/store/app.selector";
import {setAPIStatus} from "../../shared/store/app.action";
import {Appstate} from "../../shared/store/appstate";

declare var window: any;

@Component({
  selector: 'src-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
})

export class OrderComponent implements OnInit {
  title = 'GreenIT Application Challenge';
  public faEdit = faEdit;
  public faClose = faClose;
  csvData: any = [];
  public gridView: any = {};
  public templates: any = [];
  public pageSize = 10;
  public pageNumber = 1;
  public skip = 0;
  public take = 10;
  public allowUnsort = true;
  public sort: SortDescriptor[] = [];
  public pageSizes = [10, 25, 50, 100];
  submitted = false;
  private editedRowIndex: number;
  public gridData: any = {
    data: [],
    total: 0
  }
  public gridSelectRow: any = [];
  idTodelete: number = 0;
  deleteModal: any;

  public disabled = true;
  public editFromGroup: any;
  public createFromGroup: any;
  order$ = this.store.pipe(select(selectOrders));

  constructor(private orderService: OrdersService, private model: NgbModal, private appStore: Store<Appstate>,
              private toastService: ToastService, private fb: FormBuilder, private store: Store
  ) {
  }

  ngOnInit(): void {
    /* Get the Order Details while loading page */
    this.getOrderData();

    // TO get the details
    //this.store.dispatch(invokeOrdersAPI());

    this.deleteModal = new window.bootstrap.Modal(
      document.getElementById("deleteModal")
    );

  };

  /* Paging the Grid */
  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.take = event.take;
    this.pageNumber = Math.floor(this.skip / this.pageSize) + 1;
    this.pageSize = this.take;

  }

  /**
   * Triggered on sort change
   * @param sort
   */
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
  }

  /**
   * Column settings - Kendo Grid
   */
  public fileColumns = [
    {
      title: "Id",
      field: "id",
    },
    {
      title: "Name",
      field: "name",
      editor: true,
    },
    {
      title: "State",
      field: "state",
      editor: true,
    },
    {
      title: "Zip",
      field: "zip",
      editor: true,
    },
    {
      title: "Amount",
      field: "amount",
      editor: true,
    },
    {
      title: "Quantity",
      field: "quantity",
      editor: true,
    },
    {
      title: "Item",
      field: "item",
      editor: true,
    },
  ];

  /* When the application runs it is the first function
       to run Fetch records in csv
   */
  getOrderData(): any {
    this.orderService.getOrderData().subscribe((response: Order) => {
      if (response) {
        this.gridData = {
          data: response,
          length: 0
        };
        this.gridView = response;
      } else {
        this.gridSelectRow = [];
        this.toastService.errorMessage("Fetch Failed!!", 'Order');
      }
    })
  }

  /* Close the editor for the given row */
  public cancelHandler(args: CancelEvent): void {
    this.closeEditor(args.sender, args.rowIndex);
  }

  // Define all editable fields validators and default values
  // Put the row in edit mode, with the `FormGroup` build above
  public editOrderData(args: EditEvent): void {
    const {dataItem} = args;
    this.closeEditor(args.sender);
    this.editFromGroup = new FormGroup({
      id: new FormControl(dataItem.id),
      name: new FormControl(dataItem.name, [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]),
      state: new FormControl(dataItem.state, [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]),
      zip: new FormControl(dataItem.zip, [Validators.required, Validators.pattern("^[0-9]{6}$")]),
      amount: new FormControl(dataItem.amount, [Validators.required, Validators.pattern("^[1-9]\\d*(\\.\\d+)?$")]),
      quantity: new FormControl(dataItem.quantity, [Validators.required, Validators.pattern("^[0-9]{1,50}$")]),
      item: new FormControl(dataItem.item, [Validators.required, Validators.pattern("^[a-zA-Z0-9]{3,10}$")]),
    });

    this.editedRowIndex = args.rowIndex;
    args.sender.editRow(args.rowIndex, this.editFromGroup);
  }

  // Close the editor in the grid
  private closeEditor(grid: GridComponent, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
  }


  // While saving or creating the data in Grid
  public saveOrderData({sender, rowIndex, formGroup, isNew}: SaveEvent): any {
    if (!isNew) {
      // For Updating the  row
      this.store.dispatch(invokeUpdateOrderAPI({payload: {...this.editFromGroup.value}}))
      let appStatus$ = this.appStore.pipe(select(selectAppState))
      appStatus$.subscribe((data) => {
        if (data.apiStatus === 'Success') {
          this.toastService.successMessage('Order Updated successfully!', 'Order');
          this.appStore.dispatch(setAPIStatus({apiStatus: {apiStatus: '', apiResponseMessage: ''}}));
          this.getOrderData();
        }
      })
    }
    // For Add the new Row to the file
    else {
      this.store.dispatch(invokeSaveOrderAPI({payload: {...this.createFromGroup.value}}));
      let appStatus$ = this.appStore.pipe(select(selectAppState))
      appStatus$.subscribe((data) => {
        if (data.apiStatus === 'Success') {
          this.toastService.successMessage('Order Created successfully!', 'Order');
          this.appStore.dispatch(setAPIStatus({apiStatus: {apiStatus: '', apiResponseMessage: ''}}));
          this.getOrderData();
        }
      });

    }
    sender.closeRow(rowIndex);
  }

  /* Open a new item editor in the grid to  add the new element */
  public createOrder(args: AddEvent): void {
    this.closeEditor(args.sender);
    // define all editable fields validators and default values
    this.createFromGroup = this.fb.group({
      name: ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
      state: ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
      zip: ['', [Validators.required, Validators.pattern("^[0-9]{6}$")]],
      amount: ['', [Validators.required, Validators.pattern("^[1-9]\\d*(\\.\\d+)?$")]],
      quantity: ['', [Validators.required, Validators.pattern("^[0-9]{1,50}$")]],
      item: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9]{3,10}$")]]
    });
    args.sender.addRow(this.createFromGroup);
    this.createFromGroup.value.id = this.gridData.total.length + 1
  }

  // Pop up Modal to Delete
  openDeleteModal(args: RemoveEvent) {
    this.idTodelete = args.dataItem.id;
    this.deleteModal.show();
  }

  // To Delete the order
  confirmDelete() {
    this.store.dispatch(invokeDeleteOrderAPI({id: this.idTodelete}));
    let appStatus$ = this.appStore.pipe(select(selectAppState))
    appStatus$.subscribe((data) => {
      if (data.apiStatus === 'Success') {
        this.appStore.dispatch(setAPIStatus({apiStatus: {apiStatus: '', apiResponseMessage: ''}}));
        this.toastService.successMessage('Order Deleted successfully!', 'Order');
        this.deleteModal.hide();
        this.getOrderData();
      }
    })
  }

}
