import { waitForAsync, ComponentFixture, TestBed, fakeAsync} from '@angular/core/testing';
import { OrderComponent } from './order.component';
import { FormBuilder } from '@angular/forms';
import { ToastService } from "../../service/toast.service";
import { OrderService } from "../../service/order.service";
import {of} from "rxjs";
import {orderData} from "../../model/order/orderData.model"

describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;
  let fakeOrderService: jasmine.SpyObj<OrderService>;
  let fakeToastService: jasmine.SpyObj<ToastService>;
  let fakeFb: jasmine.SpyObj<FormBuilder>;
  let fakeModel: jasmine.SpyObj<orderData>;
  let order_data= [
    {id:"1",name:"Liquid Saffron",state:"NY",zip:"08998",amount:"25.43",qty:"7",item:"XCD45300"},
    {id:"2",name:"Mostly Slugs",state:"PA",zip:"19008",amount:"13.30",qty:"2",item:"AAH6748"},
    {id:"3",name:"Jump Stain",state:"CA",zip:"99388",amount:"56.00",qty:"3",item:"MKII4400"},
    {id:"4",name:"Scheckled Sherlock",state:"WA",zip:"88990",amount:"987.56",qty:"1",item:"TR909"}
  ];



  beforeEach(waitForAsync(() => {
    fakeOrderService = jasmine.createSpyObj<OrderService>('OrderService', ['getOrderData', 'updateOrderData', 'createOrderData', 'deleteOrderData']);
    fakeToastService = jasmine.createSpyObj<ToastService>('ToastService', ['successMessage', 'errorMessage']);
    fakeFb = jasmine.createSpyObj<FormBuilder>('FormBuilder', ['group']);

    TestBed.configureTestingModule({
      declarations: [OrderComponent],
      providers: [
        { provide: OrderService, useFactory: () => fakeOrderService },
        { provide: ToastService, useFactory: () => fakeToastService },
        { provide: FormBuilder, useFactory: () => fakeFb },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit', () => {

    spyOn(component,"getOrderData").and.returnValue([]);
    component.ngOnInit();

    //assert
    expect(component.getOrderData).toBeTruthy();

  });

  it('should call on delete', () => {

    // @ts-ignore
    spyOn(component,"deleteOrder").and.returnValue([]);
    component.deleteOrder();

    //assert
    expect(component.deleteOrder).toBeTruthy();

  })

});
