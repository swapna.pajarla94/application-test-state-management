import {Component, Injectable, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, ValidationErrors} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {select, Store} from "@ngrx/store";
import {ToastService} from "../toast.service";
import {Appstate} from "../../shared/store/appstate";
import {selectAppState} from "../../shared/store/app.selector";
import {setAPIStatus} from "../../shared/store/app.action";
import {invokeSaveOrderAPI, invokeUpdateOrderAPI} from "../store/order.action";
import {orderErrorMessages} from "../../shared/Error/error";
import {switchMap} from "rxjs";
import {selectOrderById} from "../store/order.selector";
import {Order} from "../store/order";

@Injectable({
  providedIn: 'root' // just before your class
})

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [NgbActiveModal, NgbModal]
})
export class CreateComponent implements OnInit {

  orderForm: FormGroup;
  public errorMessages = orderErrorMessages;
  public editForm: boolean;
  createTitle = 'Create order'
  editTitle = 'Edit order'

  /**
   * Creates an instance of create component.
   * @param activeModal
   * @param store
   * @param fb
   * @param toastService
   * @param appStore
   * @param router
   * @param model
   * @param route
   */
  constructor(
    public activeModal: NgbActiveModal,
    private store: Store, private fb: FormBuilder, private toastService: ToastService,
    private appStore: Store<Appstate>, private router: Router, private model: NgbModal, private route: ActivatedRoute,
  ) {
    // Calling the Create Form
    this.createForm();
  }

  /**
   * on init
   */
  ngOnInit() {
    // If it is Edit form the calling patching the values to form group
    this.route.paramMap.subscribe((params => {
      // @ts-ignore
      const orderId = +params.get('id');
      if (orderId) {
        this.getOrderById(orderId);
      }
    }))
  }

  // Get order  by ID
  getOrderById(id: number) {
    this.editForm = true;

    let fetchFormData$ = this.route.paramMap.pipe(
      switchMap((param) => {
        var id = Number(param.get('id'));
        return this.store.pipe(select(selectOrderById(id)))
      })
    )

    fetchFormData$.subscribe((data) => {
      if (data) {
        this.editOrder(data);
      } else {
        this.router.navigate(['/']);
      }
    });
  }

  // Patching the values to Create Form
  editOrder(data: Order) {
    this.orderForm.patchValue({
      id: data.id,
      name: data.name,
      state: data.state,
      zip: data.zip,
      amount: data.amount,
      quantity: data.quantity,
      item: data.item,
    });
  }

  // Create Form Control
  createForm() {
    this.orderForm = this.fb.group({
      id: [''],
      name: ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
      state: ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
      zip: ['', [Validators.required, Validators.pattern("^[0-9]{6}$")]],
      amount: ['', [Validators.required, Validators.pattern("^[1-9]\\d*(\\.\\d+)?$")]],
      quantity: ['', [Validators.required, Validators.pattern("^[0-9]{1,50}$")]],
      item: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9]{3,10}$")]]
    })
  }

  // To Create the Order
  create() {
    // @ts-ignore
    this.store.dispatch(invokeSaveOrderAPI({payload: {...this.orderForm.value}}));
    let appStatus$ = this.appStore.pipe(select(selectAppState))
    appStatus$.subscribe((data) => {
      if (data.apiStatus === 'Success') {
        this.toastService.successMessage('Order Created successfully!', 'Order');
        this.appStore.dispatch(setAPIStatus({apiStatus: {apiStatus: '', apiResponseMessage: ''}}));
        this.router.navigate(['/']);
      }
    });
    this.closeModal('Modal closed');
  }

  // Get for Form Validations

  get getName() {
    return this.orderForm.get('name');
  }

  get getState() {
    return this.orderForm.get('state');
  }

  get getZip() {
    return this.orderForm.get('zip');
  }

  get getAmount() {
    return this.orderForm.get('amount');
  }

  get getQuantity() {
    return this.orderForm.get('quantity');
  }

  get getItem() {
    return this.orderForm.get('item');
  }

  // Control is the form to create the order or edit the order
  submit() {
    if (!this.editForm) {
      this.create();
    } else {
      this.update();
    }
  }

  // updating the Order
  update() {
    this.store.dispatch(invokeUpdateOrderAPI({payload: {...this.orderForm.value}}))
    let appStatus$ = this.appStore.pipe(select(selectAppState))
    appStatus$.subscribe((data) => {
      if (data.apiStatus === 'Success') {
        this.activeModal.close('close');
        this.toastService.successMessage('Order Updated successfully!', 'Order');
        this.appStore.dispatch(setAPIStatus({apiStatus: {apiStatus: '', apiResponseMessage: ''}}));
        this.router.navigate(['/'])
      }
    })
  }

  /**
   * Closes modal
   * @param sendData
   */
  closeModal(sendData: string) {
    // A reference to the currently opened (active) modal, and invoke close function
    console.log('asas');
   // this.activeModal.close(sendData);
    this.router.navigate(['/'])
  }
}
