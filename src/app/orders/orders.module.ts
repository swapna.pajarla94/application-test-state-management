import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersRoutingModule } from './orders-routing.module';
import {StoreModule} from "@ngrx/store";
import {OrderReducer} from "./store/order.reducer";
import {EffectsModule} from "@ngrx/effects";
import {OrderEffects} from "./store/order.effects";
import {CreateComponent} from './create/create.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {GridModule} from "@progress/kendo-angular-grid";
import {Ng2SearchPipe, Ng2SearchPipeModule} from "ng2-search-filter";
import {Ng2OrderModule} from "ng2-order-pipe";
import {NgxPaginationModule} from "ngx-pagination";
import {AgGridModule} from "ag-grid-angular";
import {OrderComponent} from "./order/order.component";


@NgModule({
  declarations: [
    CreateComponent,
    OrderComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    StoreModule.forFeature("MyOrders", OrderReducer),
    EffectsModule.forFeature([OrderEffects]),
    FormsModule,
    ReactiveFormsModule,
    GridModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    NgxPaginationModule,
    AgGridModule
  ],
})
export class OrdersModule { }
