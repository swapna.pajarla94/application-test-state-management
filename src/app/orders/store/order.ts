export interface Order  {
  id: number;
  name: string;
  state: string;
  zip: number;
  amount: number;
  quantity: number;
  item: string;
}

export  interface  OrderState{
  orders:Order[]
}



