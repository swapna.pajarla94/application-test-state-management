import {Order} from "./order";
import {createReducer, on} from "@ngrx/store";
import {deleteOrderAPISuccess, ordersFetchAPISuccess, saveOrderAPISuccess, updateOrderAPISuccess} from "./order.action";

// It is used to call the store by accepting methods and parametrs
export const initialState: ReadonlyArray<Order> = [];

 // To get Every order
export const OrderReducer = createReducer(
  initialState,
 /* on(ordersFetchAPISuccess, (state, {allOrders}) => {
    return allOrders;
  }),*/

  // To Create the order
  on(saveOrderAPISuccess, (state, {response}) => {
    let newState = [...state];
    newState.unshift(response);
    return newState;
  }),

  // To Update the order
  on(updateOrderAPISuccess, (state, {response}) => {
    let newState = state.filter(_ => _.id !== response.id);
    newState.unshift(response);
    return newState;
  }),

  // To delete the order
  on(deleteOrderAPISuccess, (state, {id}) => {
    return state.filter(_ => _.id !== id);
  })
);
