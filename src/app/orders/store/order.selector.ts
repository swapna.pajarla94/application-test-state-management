import {createFeatureSelector, createSelector} from "@ngrx/store";
import {Order} from "./order";

export const selectOrders= createFeatureSelector<Order[]>("MyOrders")

// For Edit the order to know which function is calling
export const selectOrderById = (orderId: number) => {
  return createSelector(selectOrders, (orders: Order[])=>{
    var orderById = orders.filter(_ =>_.id == orderId);
    if(orderById.length == 0){
      return null;
    }
    return orderById[0];
    }
  )
}
