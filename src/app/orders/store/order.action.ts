import {createAction, props} from "@ngrx/store";
import {Order} from "./order";

export const invokeOrdersAPI = createAction("[Order API] invokes orders fetch API")

// Get orders  list
export const ordersFetchAPISuccess = createAction(
  "[Order API] orders fetch API success",
  props<{ allOrders: Order[] }>()
)

// Create Order
export const invokeSaveOrderAPI = createAction(
  "[Order API] invoke save order API",
  props<{ payload: Order }>()
)

// Response after creating the new order
export const saveOrderAPISuccess = createAction(
  "[Order API] save order API success",
  props<{ response: Order }>()
)

// Update the order
export const invokeUpdateOrderAPI = createAction(
  "[Order API] invoke update order API",
  props<{ payload: Order }>(),
)

//Response after updating the new order
export const updateOrderAPISuccess = createAction(
  "[Order API] update order API success",
  props<{ response: Order }>()
)

// Delete the order
export const invokeDeleteOrderAPI = createAction(
  "[Order API] invoke delete order API",
  props<{ id: number }>()
)

// Response after delete the order
export const deleteOrderAPISuccess = createAction(
  "[Order API] delete order API success",
  props<{ id: number }>()
)
