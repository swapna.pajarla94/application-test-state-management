import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {OrdersService} from "../orders.service";
import {
  deleteOrderAPISuccess,
  invokeDeleteOrderAPI,
  invokeOrdersAPI,
  invokeSaveOrderAPI,
  invokeUpdateOrderAPI,
  ordersFetchAPISuccess,
  saveOrderAPISuccess, updateOrderAPISuccess
} from "./order.action";
import {EMPTY, map, switchMap, withLatestFrom} from "rxjs";
import {Appstate} from "../../shared/store/appstate";
import {select, Store} from "@ngrx/store";
import {setAPIStatus} from "../../shared/store/app.action";
import {selectOrders} from "./order.selector";

@Injectable()
export class OrderEffects{
  constructor(private actions$: Actions,
      private orderService: OrdersService,
              private store: Store,
              private appStore:Store<Appstate>
  ) {}

/*
  // invokeOrdersAPI - Whenever the "get all order action perfoms this function will hit
  loadAllOrder$ = createEffect(()=>
    this.actions$.pipe(
      ofType(invokeOrdersAPI),
      // For not Loading every Item just to get whatever the order we are creating
      withLatestFrom(this.store.pipe(select(selectOrders))),
      switchMap(([,ordersFromStore]) => {
        if(ordersFromStore.length>0){
          return EMPTY;
        }
        return this.orderService
          .get()
          .pipe(map((data) => ordersFetchAPISuccess({allOrders: data})))
      })

    )
  )
*/

  //Reading the input method and performing the action
  saveNewOrder$ = createEffect(()=>
    this.actions$.pipe(
      ofType(invokeSaveOrderAPI),
      switchMap((action) => {
        this.appStore.dispatch(setAPIStatus({apiStatus:{apiResponseMessage:'',apiStatus: ''}}))
        return this.orderService
          .create(action.payload)
          .pipe(map((data) =>{
            this.appStore.dispatch(setAPIStatus({apiStatus:{apiResponseMessage:'', apiStatus: 'Success'}}))
            return  saveOrderAPISuccess({response: data})
          }))
      })
    )
  )


  //Reading the input method and performing the action
  updateOrder$ = createEffect(()=>
    this.actions$.pipe(
      //calling api
      ofType(invokeUpdateOrderAPI),
      switchMap((action) =>
      {
        this.appStore.dispatch(setAPIStatus({apiStatus:{apiResponseMessage:'',apiStatus: ''}}));
        return this.orderService
          .update(action.payload)
          .pipe(map((data) =>{
            this.appStore.dispatch(setAPIStatus({apiStatus:{apiResponseMessage:'', apiStatus: 'Success'}}))
            return updateOrderAPISuccess({response: data})
          }))
      })
    )
  )

  // Creating the effect to delete the orders
  deleteOrder$ = createEffect(()=>
    this.actions$.pipe(
      //calling api
      ofType(invokeDeleteOrderAPI),
      switchMap((action) =>
      {
        this.appStore.dispatch(setAPIStatus({apiStatus:{apiResponseMessage:'',apiStatus: ''}}));
        return this.orderService
          .delete(action.id)
          .pipe(map((data) =>{
            this.appStore.dispatch(setAPIStatus({apiStatus:{apiResponseMessage:'', apiStatus: 'Success'}}))
            return deleteOrderAPISuccess({id: action.id})
          }))
      })
    )
  )
}
