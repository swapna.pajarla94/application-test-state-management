import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Order} from "./store/order";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private http:HttpClient) { }

  get(){
    return this.http.get<Order[]>(`http://localhost:3000/orders`)
  }

  getOrderData(): Observable<Order> {
    return this.http.get<Order>(`http://localhost:3000/orders`)
  }

  create(payload: Order){
    return this.http.post<Order>(`http://localhost:3000/orders`, payload)
  }

  update(payload: Order){
    return this.http.put<Order>(`http://localhost:3000/orders/${payload.id}` , payload)
  }

  delete(id:number){
    return this.http.delete(`http://localhost:3000/orders/${id}`)
  }

}
