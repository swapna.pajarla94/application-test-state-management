import {createAction, props} from "@ngrx/store";
import {Appstate} from "./appstate";

// To send response Globally
export const setAPIStatus = createAction(
  '[API] Successs or failures status',
    props<{apiStatus: Appstate}>()
)
