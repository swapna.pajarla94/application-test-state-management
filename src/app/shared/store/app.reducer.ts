import {createReducer, on} from "@ngrx/store";
import {Appstate} from "./appstate";
import {state} from "@angular/animations";
import {setAPIStatus} from "./app.action";
import {ordersFetchAPISuccess} from "../../orders/store/order.action";

export const initialState: Appstate ={
  apiStatus : '',
  apiResponseMessage: ''
}

export const appReducer= createReducer(
  initialState,
  on(setAPIStatus,(state,{apiStatus}) =>{
    return apiStatus
  })
)
